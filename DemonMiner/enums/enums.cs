﻿using System.ComponentModel;

namespace DemonMiner.Enums
{

    public enum Protocol
    {
        [Description("Getwork")]
        Getwork = 0,
        [Description("Stratum Auto Negotiate")]
        Stratum = 1,
        [Description("Stratum v1")]
        Stratum1 = 2,
        [Description("Stratum v2")]
        Stratum2 = 3,
        [Description("Better Hash")]
        BetterHash = 4,
    }


}
