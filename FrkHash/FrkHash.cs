﻿//    Demon Miner - cryptocurrency miner
//    Copyright (C) 2021  Julian Rodriquez
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.


using System;

namespace FrkHash
{
    public static class HashValues
    {
        public static int Rate(int bitLength) { return (1600 - (bitLength << 1)) / 8;  }
        public static int Length(int bitLength) { return bitLength / 8; }
    }

    public static class Ketchup
    {
        // Always going to be Keccak;
        public static readonly int _hashType = 0x01;

        internal static byte[] Extract(byte[] bs, ulong[] state)
        {
            var off = 0;
            for (var i = 0; i < bs.Length / 8; i++)
            {
                bs[off] = (byte)(state[i]);
                bs[off + 1] = (byte)(state[i] >> 8);
                bs[off + 2] = (byte)(state[i] >> 16);
                bs[off + 3] = (byte)(state[i] >> 24);
                bs[off + 4] = (byte)(state[i] >> 32);
                bs[off + 5] = (byte)(state[i] >> 40);
                bs[off + 6] = (byte)(state[i] >> 48);
                bs[off + 7] = (byte)(state[i] >> 56);
                off += 8;
            }
            return bs;
        }

        internal static ulong[] Permute(ulong[] state)
        {
            ulong A00, A01, A02, A03, A04, A05, A06, A07, A08, A09, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, A21, A22, A23, A24, C0, 
                C1, c2, c3, c4, d0, d1, d2, d3, d4;
            //ulong cheatLong = 0x0000000000000001;

            A00 = state[0]; A01 = state[1]; A02 = state[2]; A03 = state[3]; A04 = state[4];
            A05 = state[5]; A06 = state[6]; A07 = state[7]; A08 = state[8]; A09 = state[9];
            A10 = state[10]; A11 = state[11]; A12 = state[12]; A13 = state[13]; A14 = state[14];
            A15 = state[15]; A16 = state[16]; A17 = state[17]; A18 = state[18]; A19 = state[19];
            A20 = state[20]; A21 = state[21]; A22 = state[22]; A23 = state[23]; A24 = state[24];

            for (var round = 0; round < 24; round++)
            {
                // Theta
                C0 = A00 ^ A05 ^ A10 ^ A15 ^ A20;
                C1 = A01 ^ A06 ^ A11 ^ A16 ^ A21;
                c2 = A02 ^ A07 ^ A12 ^ A17 ^ A22;
                c3 = A03 ^ A08 ^ A13 ^ A18 ^ A23;
                c4 = A04 ^ A09 ^ A14 ^ A19 ^ A24;

                d0 = ShiftULongLeft(C1, 1) ^ c4;
                d1 = ShiftULongLeft(c2, 1) ^ C0;
                d2 = ShiftULongLeft(c3, 1) ^ C1;
                d3 = ShiftULongLeft(c4, 1) ^ c2;
                d4 = ShiftULongLeft(C0, 1) ^ c3;

                A00 ^= d0;
                A05 ^= d0;
                A10 ^= d0;
                A15 ^= d0;
                A20 ^= d0;
                A01 ^= d1;
                A06 ^= d1;
                A11 ^= d1;
                A16 ^= d1;
                A21 ^= d1;
                A02 ^= d2;
                A07 ^= d2;
                A12 ^= d2;
                A17 ^= d2;
                A22 ^= d2;
                A03 ^= d3;
                A08 ^= d3;
                A13 ^= d3;
                A18 ^= d3;
                A23 ^= d3;
                A04 ^= d4;
                A09 ^= d4;
                A14 ^= d4;
                A19 ^= d4;
                A24 ^= d4;


                // RhoPi
                C1 = ShiftULongLeft(A01, 1);

                A01 = ShiftULongLeft(A06, 44);
                A06 = ShiftULongLeft(A09, 20);
                A09 = ShiftULongLeft(A22, 61);
                A22 = ShiftULongLeft(A14, 39);
                A14 = ShiftULongLeft(A20, 18);
                A20 = ShiftULongLeft(A02, 62);
                A02 = ShiftULongLeft(A12, 43);
                A12 = ShiftULongLeft(A13, 25);
                A13 = ShiftULongLeft(A19, 08);
                A19 = ShiftULongLeft(A23, 56);
                A23 = ShiftULongLeft(A15, 41);
                A15 = ShiftULongLeft(A04, 27);
                A04 = ShiftULongLeft(A24, 14);
                A24 = ShiftULongLeft(A21, 02);
                A21 = ShiftULongLeft(A08, 55);
                A08 = ShiftULongLeft(A16, 45);
                A16 = ShiftULongLeft(A05, 36);
                A05 = ShiftULongLeft(A03, 28);
                A03 = ShiftULongLeft(A18, 21);
                A18 = ShiftULongLeft(A17, 15);
                A17 = ShiftULongLeft(A11, 10);
                A11 = ShiftULongLeft(A07, 06);
                A07 = ShiftULongLeft(A10, 03);

                A10 = C1;

                //Chi
                C0 = A00 ^ (~A01 & A02);
                C1 = A01 ^ (~A02 & A03);
                A02 ^= ~A03 & A04;
                A03 ^= ~A04 & A00;
                A04 ^= ~A00 & A01;
                A00 = C0;
                A01 = C1;

                C0 = A05 ^ (~A06 & A07);
                C1 = A06 ^ (~A07 & A08);
                A07 ^= ~A08 & A09;
                A08 ^= ~A09 & A05;
                A09 ^= ~A05 & A06;
                A05 = C0;
                A06 = C1;

                C0 = A10 ^ (~A11 & A12);
                C1 = A11 ^ (~A12 & A13);
                A12 ^= ~A13 & A14;
                A13 ^= ~A14 & A10;
                A14 ^= ~A10 & A11;
                A10 = C0;
                A11 = C1;

                C0 = A15 ^ (~A16 & A17);
                C1 = A16 ^ (~A17 & A18);
                A17 ^= ~A18 & A19;
                A18 ^= ~A19 & A15;
                A19 ^= ~A15 & A16;
                A15 = C0;
                A16 = C1;

                C0 = A20 ^ (~A21 & A22);
                C1 = A21 ^ (~A22 & A23);
                A22 ^= ~A23 & A24;
                A23 ^= ~A24 & A20;
                A24 ^= ~A20 & A21;
                A20 = C0;
                A21 = C1;


                // Iota
                ulong cheatLong = 0x0000000000000001;
                
                switch (round) {
                    case 0:
                        cheatLong = 0x0000000000000001;
                        break;
                    case 1:
                        cheatLong = 0x0000000000008082;
                        break;
                    case 2:
                        cheatLong = 0x800000000000808A;
                        break;
                    case 3:
                        cheatLong = 0x8000000080008000;
                        break;
                    case 4:
                        cheatLong = 0x000000000000808B;
                        break;
                    case 5:
                        cheatLong = 0x0000000080000001;
                        break;
                    case 6:
                        cheatLong = 0x8000000080008081;
                        break;
                    case 7:
                        cheatLong = 0x8000000000008009;
                        break;
                    case 8:
                        cheatLong = 0x000000000000008A;
                        break;
                    case 9:
                        cheatLong = 0x0000000000000088;
                        break;
                    case 10:
                        cheatLong = 0x0000000080008009;
                        break;
                    case 11:
                        cheatLong = 0x000000008000000A;
                        break;
                    case 12:
                        cheatLong = 0x000000008000808B;
                        break;
                    case 13:
                        cheatLong = 0x800000000000008B;
                        break;
                    case 14:
                        cheatLong = 0x8000000000008089;
                        break;
                    case 15:
                        cheatLong = 0x8000000000008003;
                        break;
                    case 16:
                        cheatLong = 0x8000000000008002;
                        break;
                    case 17:
                        cheatLong = 0x8000000000000080;
                        break;
                    case 18:
                        cheatLong = 0x000000000000800A;
                        break;
                    case 19:
                        cheatLong = 0x800000008000000A;
                        break;
                    case 20:
                        cheatLong = 0x8000000080008081;
                        break;
                    case 21:
                        cheatLong = 0x8000000000008080;
                        break;
                    case 22:
                        cheatLong = 0x0000000080000001;
                        break;
                    case 23:
                        cheatLong = 0x8000000080008008;
                        break;
                    default:
                        cheatLong = 0x0000000000000001;
                        break;

                }
                A00 ^= cheatLong;//RoundConstants[round];
            }

            state[0] = A00; state[1] = A01; state[2] = A02; state[3] = A03; state[4] = A04;
            state[5] = A05; state[6] = A06; state[7] = A07; state[8] = A08; state[9] = A09;
            state[10] = A10; state[11] = A11; state[12] = A12; state[13] = A13; state[14] = A14;
            state[15] = A15; state[16] = A16; state[17] = A17; state[18] = A18; state[19] = A19;
            state[20] = A20; state[21] = A21; state[22] = A22; state[23] = A23; state[24] = A24;

            return state;
        }



        public static byte[] ArrayFeed(byte[] Source, int sIndex,byte[] Dest, int dIndex, int Range )
        {
            for(int i = 0; i < Range; i++)
            {
                Dest[i + dIndex] = Source[i + sIndex];
            }
            return Dest;
        }

        public static int iRate(int bitLength) { return (1600 - (bitLength << 1)) / 8; }
        public static int iLength(int bitLength) { return bitLength / 8; }

        
        public static byte[] Hash(byte[] bytesToHash, int bitLength = 256)
        {
            int blockSize = default;
            int Rate = iRate(bitLength);
            int Length = iLength(bitLength);
            ulong[] state = new ulong[25];
            byte[] result = new byte[Length];
            byte[] extracted = new byte[Rate];
            int size = bytesToHash.Length;
            var counter = 0;
            int offSet;

            while (size > 0)

            {
                blockSize = Math.Min(size, Rate);


                for (var i = 0; i < blockSize / 8; i++)
                {
                    offSet = i * 8;
                    state[i] ^= AddStateBuffer(bytesToHash, offSet);
                }
                size -= blockSize;

                if (blockSize != Rate) continue;
                state = Permute(state);
                counter += Rate;
                blockSize = 0;
            }
            size = bytesToHash.Length;

            var mod = (size % Rate) % 8;
            var finalRound = (size % Rate) / 8;
            var partial = new byte[8];

            partial = ArrayFeed(bytesToHash, size - mod, partial, 0, mod);
            //Buffer.BlockCopy(bytesToHash, size - mod, partial, 0, mod);
            partial[mod] = 0x01;

            state[finalRound] ^= AddStateBuffer(partial, 0);
            state[(Rate - 1) >> 3] ^= (1UL << 63);
            state = Permute(state);

            extracted = Extract(extracted, state);
            var outputBytesLeft = Length;

            while (outputBytesLeft > 0)
            {
                blockSize = Math.Min(outputBytesLeft, Rate);
                result = ArrayFeed(extracted, 0, result, 0, blockSize);
                //Buffer.BlockCopy(extracted,0,result,0, blockSize);
                outputBytesLeft -= blockSize;

                if (outputBytesLeft <= 0) continue;
                state = Permute(state);
            }

            return result;

        }

        internal static ulong AddStateBuffer(byte[] bs, int off)
        {
            var result = bs[off]
                | (ulong)bs[off + 1] << 8
                | (ulong)bs[off + 2] << 16
                | (ulong)bs[off + 3] << 24
                | (ulong)bs[off + 4] << 32
                | (ulong)bs[off + 5] << 40
                | (ulong)bs[off + 6] << 48
                | (ulong)bs[off + 7] << 56;

            return result;
        }
        
        internal static readonly ulong[] RoundConstants = new ulong[]
        {
            0x0000000000000001, 0x0000000000008082, 0x800000000000808A, 0x8000000080008000,
            0x000000000000808B, 0x0000000080000001, 0x8000000080008081, 0x8000000000008009,
            0x000000000000008A, 0x0000000000000088, 0x0000000080008009, 0x000000008000000A,
            0x000000008000808B, 0x800000000000008B, 0x8000000000008089, 0x8000000000008003,
            0x8000000000008002, 0x8000000000000080, 0x000000000000800A, 0x800000008000000A,
            0x8000000080008081, 0x8000000000008080, 0x0000000080000001, 0x8000000080008008
        };


        private static ulong ShiftULongLeft(ulong x, byte y) => (x << y) | (x >> (64 - y));
    }
}
