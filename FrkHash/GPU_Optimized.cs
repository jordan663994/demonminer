﻿namespace FrkHash
{
    public static class Algorithms
    {
        public static byte[] Optimized_Hash(byte[] bytesToHash,int bitLen, int hashLen = -1)
        {
            
            
            int blockSize = default;
            int Rate = (1600 - (bitLen << 1)) / 8;
            int Length = bitLen / 8;
            ulong[] state = new ulong[25];
            byte[] result = new byte[Length];
            byte[] extracted = new byte[Rate];
            int sizeConst = hashLen <= 0 ? bytesToHash.Length : hashLen;
            int size = sizeConst;
            var counter = 0;
            int offSet;

            while (size > 0)

            {
                if(size < Rate)
                {
                    blockSize = size;
                } else
                {
                    blockSize = Rate;
                }


                for (var i = 0; i < blockSize / 8; i++)
                {
                    offSet = i * 8;
                    state[i] ^= bytesToHash[offSet]
                        | (ulong)bytesToHash[offSet + 1] << 8
                        | (ulong)bytesToHash[offSet + 2] << 16
                        | (ulong)bytesToHash[offSet + 3] << 24
                        | (ulong)bytesToHash[offSet + 4] << 32
                        | (ulong)bytesToHash[offSet + 5] << 40
                        | (ulong)bytesToHash[offSet + 6] << 48
                        | (ulong)bytesToHash[offSet + 7] << 56;
                }
                size -= blockSize;

                if (blockSize != Rate) continue;
                state = Permute(state);
                counter += Rate;
                blockSize = 0;
            }
            size = sizeConst;
            
            var mod = (size % Rate) % 8;
            var finalRound = (size % Rate) / 8;
            var partial = new byte[8];

            partial = ArrayFeed(bytesToHash, size - mod, partial, 0, mod);
            //Buffer.BlockCopy(bytesToHash, size - mod, partial, 0, mod);
            partial[mod] = 0x01;

            state[finalRound] ^= partial[0]
                | (ulong)partial[1] << 8
                | (ulong)partial[2] << 16
                | (ulong)partial[3] << 24
                | (ulong)partial[4] << 32
                | (ulong)partial[5] << 40
                | (ulong)partial[6] << 48
                | (ulong)partial[7] << 56;
            state[(Rate - 1) >> 3] ^= (1UL << 63);
            state = Permute(state);

            extracted = Extract(extracted, state);
            var outputBytesLeft = Length;

            while (outputBytesLeft > 0)
            {
                if (outputBytesLeft < Rate)
                {
                    blockSize = outputBytesLeft;
                }
                else
                {
                    blockSize = Rate;
                }
                result = ArrayFeed(extracted, 0, result, 0, blockSize);
                //Buffer.BlockCopy(extracted,0,result,0, blockSize);
                outputBytesLeft -= blockSize;

                if (outputBytesLeft <= 0) continue;
                state = Permute(state);
            }
            
            return result;
        }

        public static byte[] Optimized_Hash_512(byte[] bytesToHash)
        {
            ulong[] state = new ulong[25];
            byte[] result = new byte[64];
            byte[] extracted = new byte[72];
            int offSet;


            for (var i = 0; i < 4; i++)
            {
                offSet = i * 8;
                state[i] ^= bytesToHash[offSet]
                | (ulong)bytesToHash[offSet + 1] << 8
                | (ulong)bytesToHash[offSet + 2] << 16
                | (ulong)bytesToHash[offSet + 3] << 24
                | (ulong)bytesToHash[offSet + 4] << 32
                | (ulong)bytesToHash[offSet + 5] << 40
                | (ulong)bytesToHash[offSet + 6] << 48
                | (ulong)bytesToHash[offSet + 7] << 56;
            }

            var partial = new byte[8];

            partial[0] = 0x01;

            state[4] ^= partial[0]
                | (ulong)partial[1] << 8
                | (ulong)partial[2] << 16
                | (ulong)partial[3] << 24
                | (ulong)partial[4] << 32
                | (ulong)partial[5] << 40
                | (ulong)partial[6] << 48
                | (ulong)partial[7] << 56;
            state[8] ^= (ulong)9223372036854775808;

            state = Permute(state);

            for (var i = 0; i < 9; i++)
            {
                offSet = i * 8;
                extracted[offSet] = (byte)(state[i]);
                extracted[offSet + 1] = (byte)(state[i] >> 8);
                extracted[offSet + 2] = (byte)(state[i] >> 16);
                extracted[offSet + 3] = (byte)(state[i] >> 24);
                extracted[offSet + 4] = (byte)(state[i] >> 32);
                extracted[offSet + 5] = (byte)(state[i] >> 40);
                extracted[offSet + 6] = (byte)(state[i] >> 48);
                extracted[offSet + 7] = (byte)(state[i] >> 56);

            }
            result = ArrayFeed(extracted, 0, result, 0, 64);
            state = Permute(state);
            result = ArrayFeed(extracted, 0, result, 0, 32);

            return result;
        }



        public static ulong[] Permute(ulong[] state)
        {

            ulong A00, A01, A02, A03, A04, A05, A06, A07, A08, A09, A10, A11, A12, A13, A14, A15, A16, A17, A18, A19, A20, A21, A22, A23, A24, C0,
    C1, c2, c3, c4, d0, d1, d2, d3, d4;

            A00 = state[0]; A01 = state[1]; A02 = state[2]; A03 = state[3]; A04 = state[4];
            A05 = state[5]; A06 = state[6]; A07 = state[7]; A08 = state[8]; A09 = state[9];
            A10 = state[10]; A11 = state[11]; A12 = state[12]; A13 = state[13]; A14 = state[14];
            A15 = state[15]; A16 = state[16]; A17 = state[17]; A18 = state[18]; A19 = state[19];
            A20 = state[20]; A21 = state[21]; A22 = state[22]; A23 = state[23]; A24 = state[24];

            for (var round = 0; round < 24; round++)
            {
                // Theta
                C0 = A00 ^ A05 ^ A10 ^ A15 ^ A20;
                C1 = A01 ^ A06 ^ A11 ^ A16 ^ A21;
                c2 = A02 ^ A07 ^ A12 ^ A17 ^ A22;
                c3 = A03 ^ A08 ^ A13 ^ A18 ^ A23;
                c4 = A04 ^ A09 ^ A14 ^ A19 ^ A24;
                
                d0 = ((C1 << 1) | (C1 >> (64 - 1))) ^ c4;
                d1 = ((c2 << 1) | (c2 >> (64 - 1))) ^ C0;
                d2 = ((c3 << 1) | (c3 >> (64 - 1))) ^ C1;
                d3 = ((c4 << 1) | (c4 >> (64 - 1))) ^ c2;
                d4 = ((C0 << 1) | (C0 >> (64 - 1))) ^ c3;
                
                A00 ^= d0;
                A05 ^= d0;
                A10 ^= d0;
                A15 ^= d0;
                A20 ^= d0;
                A01 ^= d1;
                A06 ^= d1;
                A11 ^= d1;
                A16 ^= d1;
                A21 ^= d1;
                A02 ^= d2;
                A07 ^= d2;
                A12 ^= d2;
                A17 ^= d2;
                A22 ^= d2;
                A03 ^= d3;
                A08 ^= d3;
                A13 ^= d3;
                A18 ^= d3;
                A23 ^= d3;
                A04 ^= d4;
                A09 ^= d4;
                A14 ^= d4;
                A19 ^= d4;
                A24 ^= d4;


                // RhoPi
                C1 = (A01 << 1) | (A01 >> (64 - 1));

                A01 = (A06 << 44) | (A06 >> (64 - 44));
                A06 = (A09 << 20) | (A09 >> (64 - 20));
                A09 = (A22 << 61) | (A22 >> (64 - 61));
                A22 = (A14 << 39) | (A14 >> (64 - 39));
                A14 = (A20 << 18) | (A20 >> (64 - 18));
                A20 = (A02 << 62) | (A02 >> (64 - 62));
                A02 = (A12 << 43) | (A12 >> (64 - 43));
                A12 = (A13 << 25) | (A13 >> (64 - 25));
                A13 = (A19 << 08) | (A19 >> (64 - 08));
                A19 = (A23 << 56) | (A23 >> (64 - 56));
                A23 = (A15 << 41) | (A15 >> (64 - 41));
                A15 = (A04 << 27) | (A04 >> (64 - 27));
                A04 = (A24 << 14) | (A24 >> (64 - 14));
                A24 = (A21 << 02) | (A21 >> (64 - 02));
                A21 = (A08 << 55) | (A08 >> (64 - 55));
                A08 = (A16 << 45) | (A16 >> (64 - 45));
                A16 = (A05 << 36) | (A05 >> (64 - 36));
                A05 = (A03 << 28) | (A03 >> (64 - 28));
                A03 = (A18 << 21) | (A18 >> (64 - 21));
                A18 = (A17 << 15) | (A17 >> (64 - 15));
                A17 = (A11 << 10) | (A11 >> (64 - 10));
                A11 = (A07 << 06) | (A07 >> (64 - 06));
                A07 = (A10 << 03) | (A10 >> (64 - 03));

                A10 = C1;

                //Chi
                C0 = A00 ^ (~A01 & A02);
                C1 = A01 ^ (~A02 & A03);
                A02 ^= ~A03 & A04;
                A03 ^= ~A04 & A00;
                A04 ^= ~A00 & A01;
                A00 = C0;
                A01 = C1;

                C0 = A05 ^ (~A06 & A07);
                C1 = A06 ^ (~A07 & A08);
                A07 ^= ~A08 & A09;
                A08 ^= ~A09 & A05;
                A09 ^= ~A05 & A06;
                A05 = C0;
                A06 = C1;

                C0 = A10 ^ (~A11 & A12);
                C1 = A11 ^ (~A12 & A13);
                A12 ^= ~A13 & A14;
                A13 ^= ~A14 & A10;
                A14 ^= ~A10 & A11;
                A10 = C0;
                A11 = C1;

                C0 = A15 ^ (~A16 & A17);
                C1 = A16 ^ (~A17 & A18);
                A17 ^= ~A18 & A19;
                A18 ^= ~A19 & A15;
                A19 ^= ~A15 & A16;
                A15 = C0;
                A16 = C1;

                C0 = A20 ^ (~A21 & A22);
                C1 = A21 ^ (~A22 & A23);
                A22 ^= ~A23 & A24;
                A23 ^= ~A24 & A20;
                A24 ^= ~A20 & A21;
                A20 = C0;
                A21 = C1;


                // Iota
                ulong cheatLong = 0x0000000000000001;

                switch (round)
                {
                    case 0:
                        cheatLong = 0x0000000000000001;
                        break;
                    case 1:
                        cheatLong = 0x0000000000008082;
                        break;
                    case 2:
                        cheatLong = 0x800000000000808A;
                        break;
                    case 3:
                        cheatLong = 0x8000000080008000;
                        break;
                    case 4:
                        cheatLong = 0x000000000000808B;
                        break;
                    case 5:
                        cheatLong = 0x0000000080000001;
                        break;
                    case 6:
                        cheatLong = 0x8000000080008081;
                        break;
                    case 7:
                        cheatLong = 0x8000000000008009;
                        break;
                    case 8:
                        cheatLong = 0x000000000000008A;
                        break;
                    case 9:
                        cheatLong = 0x0000000000000088;
                        break;
                    case 10:
                        cheatLong = 0x0000000080008009;
                        break;
                    case 11:
                        cheatLong = 0x000000008000000A;
                        break;
                    case 12:
                        cheatLong = 0x000000008000808B;
                        break;
                    case 13:
                        cheatLong = 0x800000000000008B;
                        break;
                    case 14:
                        cheatLong = 0x8000000000008089;
                        break;
                    case 15:
                        cheatLong = 0x8000000000008003;
                        break;
                    case 16:
                        cheatLong = 0x8000000000008002;
                        break;
                    case 17:
                        cheatLong = 0x8000000000000080;
                        break;
                    case 18:
                        cheatLong = 0x000000000000800A;
                        break;
                    case 19:
                        cheatLong = 0x800000008000000A;
                        break;
                    case 20:
                        cheatLong = 0x8000000080008081;
                        break;
                    case 21:
                        cheatLong = 0x8000000000008080;
                        break;
                    case 22:
                        cheatLong = 0x0000000080000001;
                        break;
                    case 23:
                        cheatLong = 0x8000000080008008;
                        break;
                    default:
                        cheatLong = 0x0000000000000001;
                        break;

                }
                A00 ^= cheatLong;
            }

            state[0] = A00; state[1] = A01; state[2] = A02; state[3] = A03; state[4] = A04;
            state[5] = A05; state[6] = A06; state[7] = A07; state[8] = A08; state[9] = A09;
            state[10] = A10; state[11] = A11; state[12] = A12; state[13] = A13; state[14] = A14;
            state[15] = A15; state[16] = A16; state[17] = A17; state[18] = A18; state[19] = A19;
            state[20] = A20; state[21] = A21; state[22] = A22; state[23] = A23; state[24] = A24;

            return state;
        }
        public static ulong ShiftULongLeft(ulong x, byte y)
        {
            return (x << y) | (x >> (64 - y));
        }

        public static byte[] ArrayFeed(byte[] Source, int sIndex, byte[] Dest, int dIndex, int Range)
        {
            for (int i = 0; i < Range; i++)
            {
                Dest[i + dIndex] = Source[i + sIndex];
            }
            return Dest;
        }

        internal static byte[] Extract(byte[] bs, ulong[] state)
        {
            var off = 0;
            for (var i = 0; i < bs.Length / 8; i++)
            {
                bs[off] = (byte)(state[i]);
                bs[off + 1] = (byte)(state[i] >> 8);
                bs[off + 2] = (byte)(state[i] >> 16);
                bs[off + 3] = (byte)(state[i] >> 24);
                bs[off + 4] = (byte)(state[i] >> 32);
                bs[off + 5] = (byte)(state[i] >> 40);
                bs[off + 6] = (byte)(state[i] >> 48);
                bs[off + 7] = (byte)(state[i] >> 56);
                off += 8;
            }
            return bs;
        }
    }


}
