﻿//    Demon Miner - cryptocurrency miner
//    Copyright (C) 2021  Julian Rodriquez
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.



using System;
using System.Collections.Generic;
using System.Text;
using DemonMiner.Enums;

namespace DemonMiner.lib
{
    class ConnectionInfo
    {
        private string MiningURL;
        private string MiningUser;
        private string MiningPassword;
        private string MiningAddress;
        private string MiningWorker;
        private int MiningPort;
        private Protocol MiningProtocol;

        public ConnectionInfo(string url, int port, Protocol protocol = Protocol.Getwork, string address = "", string user = "", string pasword = "", string worker = "")
        {
            MiningURL = url;
            MiningUser = user;
            MiningPassword = pasword;
            MiningAddress = address.ToLower();
            MiningWorker = worker;
            MiningPort = port;
            MiningProtocol = protocol;
            ConsoleColor defaultColor = Console.ForegroundColor;
            if(MiningAddress != "" && (!MiningAddress.StartsWith('0') || !IsHex(MiningAddress.TrimStart('0').TrimStart('x')) || MiningAddress.Length != 42))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: ");
                Console.ForegroundColor = defaultColor;
                Console.Write("Invalid payment address.");
                Environment.Exit(0);
            }
        }

        private bool IsHex(IEnumerable<char> chars)
        {
            bool isHex;
            foreach (var c in chars)
            {
                isHex = ((c >= '0' && c <= '9') ||
                         (c >= 'a' && c <= 'f') ||
                         (c >= 'A' && c <= 'F'));

                if (!isHex)
                    return false;
            }
            return true;
        }

    }
}
