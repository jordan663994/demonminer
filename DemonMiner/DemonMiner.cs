﻿//    Demon Miner - cryptocurrency miner
//    Copyright (C) 2021  Julian Rodriquez
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.


using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Globalization;
using System.Numerics;
using System.Security.Cryptography;
using DemonMiner.lib;
using System.Runtime.CompilerServices;
using FrkHash;
using System.Reflection;
using System.Diagnostics;


namespace DemonMiner
{


    public class GetworkResponse
    {
        public string jsonrpc { get; set; }
        public uint id { get; set; }
        public string[] result { get; set; }
    }

    public class Workload
    {
        public string BlockHeaderString { get; set; }
        public string ProofOfWorkDifficultyString { get; set; }

        public byte[] BlockHeader { get; set; }

        public BigInteger ProofOfWorkDifficulty { get; set; }
        public byte[] PoWBytes { get; set; }
        public ulong Target { get; set; }


        public Workload(string _blockheader, string _powdifficulty)
        {
            this.BlockHeaderString = _blockheader;
            this.ProofOfWorkDifficultyString = _powdifficulty.TrimStart('0').TrimStart('x');

            this.BlockHeader = BigInteger.Parse(_blockheader.TrimStart('0').TrimStart('x'), NumberStyles.HexNumber).ToByteArray(false,true) ;
            this.ProofOfWorkDifficulty = BigInteger.Parse(_powdifficulty.TrimStart('0').TrimStart('x'), NumberStyles.HexNumber);
            this.PoWBytes = new byte[32];
            byte[] amByte = ProofOfWorkDifficulty.ToByteArray(true,true);
            for(int i=31; i >=0; i--)
            {
                if(i >= (32 - amByte.Length))
                {
                    this.PoWBytes[i] = amByte[i - (32 - amByte.Length)];
                } else
                {
                    this.PoWBytes[i] = 0x00;
                }
                
            }
            while(this.ProofOfWorkDifficultyString.Length < 64)
            {
                this.ProofOfWorkDifficultyString += "0";
            }
            string PoWDumb = ProofOfWorkDifficultyString.Substring(3, 8);
            Target = (ulong)BigInteger.Parse(PoWDumb, NumberStyles.HexNumber);
            
        }

        public static byte[] CalculateNonce()
        {
            //Allocate a buffer
            var ByteArray = new byte[8];
            //Generate a cryptographically random set of bytes
            using (var Rnd = RandomNumberGenerator.Create())
            {
                Rnd.GetBytes(ByteArray);
                
            }
            
            return ByteArray;
        }
    }

    class DemonMiner
    {



        public static string version;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte[] ConvertStringToBytes(string hash)
        {
            return Encoding.ASCII.GetBytes(hash);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static string ConvertBytesToStringHash(byte[] hashBytes)
        {
            return BitConverter.ToString(hashBytes).Replace("-", string.Empty).ToLower();
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static int ConvertBitLengthToRate(int bitLength)
        {
            return (1600 - (bitLength << 1)) / 8;
        }
        private static readonly HttpClient client = new HttpClient();


        static void Main(string[] args)
        {
            // Get the current version of the miner.

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            version = fileVersionInfo.ProductVersion;

            // Set console text encoding just to make life easier.
            Console.OutputEncoding = Encoding.UTF8;

            // First things first, say hello.

            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Demon Miner ");
            Console.ResetColor();
            Console.Write("v" + version + "\n");

            // You are required to keep this intact and display it to the user. Modification outside of the official repo revokes your license.
            Console.WriteLine("Released under GPL v3.0");
            Console.WriteLine("Official Repo: https://bitbucket.org/galactic-expanse/demonminer/src/master/ \n");
            // You may continue to wreck things now.

            if (args.Length > 0)
            {
                if (args[0].ToLower() == "-help" || args[0].ToLower() == "-?")
                {
                    Help.printManual();
                }
            }
            

            Console.WriteLine(ProcessRepositories());
        }

        private static string ProcessRepositories()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("User-Agent", "Demon Miner v" + version);


            var stringTask = client.PostAsync("http://192.168.1.152:9656", new StringContent("{ \"jsonrpc\":\"2.0\", \"method\":\"eth_getWork\", \"params\":[], \"id\":73 }", Encoding.UTF8, "application/json"));

            string msg = stringTask.Result.Content.ReadAsStringAsync().Result;
            return ParseWorkload(msg);

        }

        private static string ParseWorkload(string content)
        {
            Workload Work;
            GetworkResponse ResponseContent = JsonSerializer.Deserialize<GetworkResponse>(content);
            if (ResponseContent.result.Length >= 3)
            {
                Work = new Workload(ResponseContent.result[0], ResponseContent.result[2]);
            }
            else
            {
                return "";
            }
            return StartWork(Work);
        }

        public static string StartWork(Workload work)
        {
            Miner.CudaMinerThread(0, BitConverter.ToInt64(Workload.CalculateNonce()), 500000000);


            byte[] nonce;
            nonce = Workload.CalculateNonce();
            byte[] inonce = (new BigInteger(12345)).ToByteArray();

            // Hash concatenation of block header and nonce with Keccak512
            byte[] ConcatData = ConcatArrays(work.BlockHeader, inonce);
            byte[] seedHash = Ketchup.Hash(ConcatData, 512);

            // Hash the results from the first hash using Keccak256
            byte[] hash = Ketchup.Hash(seedHash, 256);
            //
            DateTime StartTime = DateTime.Now;

            nonce = Workload.CalculateNonce();
            
            int zeros = work.ProofOfWorkDifficultyString.Length - work.ProofOfWorkDifficultyString.TrimStart('0').Length + 1;
            //Console.WriteLine("WorkHeader Length: " + work.BlockHeader.Length);
            Parallel.For(0, 1000000, (i, state) =>
            {

                // Get a 64 byte nonce
                byte[] inonce = (new BigInteger(nonce) + i).ToByteArray();

                // Hash concatenation of block header and nonce with Keccak512
                byte[] ConcatData = ConcatArrays(work.BlockHeader, inonce);

                // Seed hash 
                byte[] seedHash = Ketchup.Hash(ConcatData, 512);

                // Hash the results from the first hash using Keccak256
                byte[] hash = Ketchup.Hash(seedHash, 256);

                // Verify the hash and send solution
                
                if (new BigInteger(hash,true,true) < work.ProofOfWorkDifficulty)
                {
                    // Convert nonce to hex number
                    string nonceString = (new BigInteger(inonce).ToString("x16"));
                    // Get the mix hash.

                    string mix = ConvertBytesToStringHash(seedHash).Substring(64);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("User-Agent", "Demon Miner v0.0.1");

                    StringContent SendContent = new StringContent("{ \"jsonrpc\":\"2.0\", \"method\":\"eth_submitWork\", \"params\":[\"0x" + nonceString + "\",\"" + work.BlockHeaderString + "\", \"0x" + mix + "\"    ], \"id\":73 }", Encoding.UTF8, "application/json");

                    var stringTask = client.PostAsync("http://192.168.1.152:9656", SendContent);

                    string msg = stringTask.Result.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Difficulty: " + work.ProofOfWorkDifficultyString + "\nHeader: " + work.BlockHeaderString + "\nNonce: " + nonceString + "\nSeed: " + ConvertBytesToStringHash(seedHash) + "\nHash: " + ConvertBytesToStringHash(hash) + "\nMix: " + mix + "\nMessage: " + msg);
                    
                }


            });

            TimeSpan Duration = DateTime.Now - StartTime;
            return ((1) / (Duration.TotalSeconds)).ToString() + " Mh/s";

        }

        public static byte[] ConcatArrays(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

    }


}

